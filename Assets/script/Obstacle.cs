﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

	float delta = -0.1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float newXPosition = transform.position.x + delta;
		if (newXPosition < -3.5) {
			delta = delta * -1;
		} else if (newXPosition > 3.5) {
			delta = delta * -1;
		}

		transform.position = new Vector3 (newXPosition, 2, -7);
	}
}
